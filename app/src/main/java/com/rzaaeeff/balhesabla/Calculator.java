package com.rzaaeeff.balhesabla;

/**
 * Created by Rzaaeeff on 28.08.2016.
 */

public class Calculator {
    /*
     * I did not use full&obvious names for these variables.
     * Because, it was going to be bulk amount of code :)
     * Here is simple explanation: [Naming Convention]
     * "C" stands for Correct ("W" for Wrong)
     * "C1" is the count of correct answers
     * in the SUBJECT 1 (and W1 ......)
     */
    private short mC1, mC2, mC3, mC4, mC5;
    private short mW1, mW2, mW3, mW4, mW5;

    public Calculator (short c1, short c2, short c3, short c4, short c5,
                       short w1, short w2, short w3, short w4, short w5) {
        mC1 = c1;
        mC2 = c2;
        mC3 = c3;
        mC4 = c4;
        mC5 = c5;
        mW1 = w1;
        mW2 = w2;
        mW3 = w3;
        mW4 = w4;
        mW5 = w5;
    }

    /**
     * Since max result is not exceeding 700
     * We don't need to put this code
     * within any Thread or Asynctask
     * or whatsoever
     *
     * Nisbi Bal :)
     * @return
     */
    public short[] calculateScore() {
        short s1, s2, s3, s4, s5;
        s1 = (short) (mC1 * 4 - mW1);
        s2 = (short) (mC2 * 4 - mW2);
        s3 = (short) (mC3 * 4 - mW3);
        s4 = (short) (mC4 * 4 - mW4);
        s5 = (short) (mC5 * 4 - mW5);

        if (s1 < 0) s1 = 0;
        if (s2 < 0) s2 = 0;
        if (s3 < 0) s3 = 0;
        if (s4 < 0) s4 = 0;
        if (s5 < 0) s5 = 0;

        return new short[]{s1, s2, s3, s4, s5};
    }

    /**
     * We multiply main subjects by 2.
     *
     * Tam bal :)
     * @return
     */
    public short calculate(){
        short result;
        short[] array = calculateScore();

        result = (short) (array[0] + 2*array[1] +
                2*array[2] + array[3] + array[4]);

        return result;
    }
}
