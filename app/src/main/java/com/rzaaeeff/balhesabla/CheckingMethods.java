package com.rzaaeeff.balhesabla;

import android.content.Context;

/**
 * Created by Rzaaeeff on 28.08.2016.
 */

public class CheckingMethods {

    private static boolean check1(short c, short w) {
        if (c + w <= 25) return true;
        return false;
    }

    public static String checkAll(Context ctx, short c1, short c2, short c3, short c4, short c5,
                                  short w1, short w2, short w3, short w4, short w5) {
        String errorDesc = "NoErrors";

        if (!check1(c1, w1)) errorDesc = ctx.getString(R.string.error_1);
        if (!check1(c2, w2)) errorDesc = ctx.getString(R.string.error_2);
        if (!check1(c3, w3)) errorDesc = ctx.getString(R.string.error_3);
        if (!check1(c4, w4)) errorDesc = ctx.getString(R.string.error_4);
        if (!check1(c5, w5)) errorDesc = ctx.getString(R.string.error_5);

        return errorDesc;
    }

    public static boolean isThereError(short c1, short c2, short c3, short c4, short c5,
                                    short w1, short w2, short w3, short w4, short w5) {
        boolean error = false;

        if (!check1(c1, w1)) error = true;
        if (!check1(c2, w2)) error = true;
        if (!check1(c3, w3)) error = true;
        if (!check1(c4, w4)) error = true;
        if (!check1(c5, w5)) error = true;

        return error;
    }
}
