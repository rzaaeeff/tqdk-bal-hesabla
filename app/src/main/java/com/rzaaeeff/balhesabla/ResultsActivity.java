package com.rzaaeeff.balhesabla;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ResultsActivity extends AppCompatActivity {

    final String PREF_NAME = "BalHesablaResults";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        SharedPreferences preferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        String name = preferences.getString("Name", getString(R.string.default_result_name_text));
        String group = preferences.getString("Group", getString(R.string.default_result_group_text));
        String result = preferences.getString("Result", getString(R.string.default_result_result_text));

        TextView nameView = (TextView) findViewById(R.id.result_name_view);
        TextView groupView = (TextView) findViewById(R.id.result_group_view);
        TextView resultView = (TextView) findViewById(R.id.result_result_view);

        nameView.setText(name);
        groupView.setText(group);
        resultView.setText(result);

        // StatusBar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Activity activity = this;
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(activity.getResources().getColor(R.color.groupStatusBarColor));
        }
    }
}
