package com.rzaaeeff.balhesabla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class InfoDialogActivity extends AppCompatActivity {
    String[] subjects;
    short[] cw;
    short[] results;

    TextView info_text_view;
    String info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_dialog);

        // Getting Extras
        subjects = getIntent().getStringArrayExtra("SUBJECTS");
        cw = getIntent().getShortArrayExtra("CW");

        // Calculating results
        Calculator calculator = new Calculator(cw[0], cw[1], cw[2], cw[3], cw[4],
                cw[5], cw[6], cw[7], cw[8], cw[9]);
        results = calculator.calculateScore();

        // Showing results on TextView
        info_text_view = (TextView) findViewById(R.id.info_dialog_text_view);

        info = getString(R.string.info_dialog_1);
        info += subjects[0].replaceAll(String.valueOf(Character.valueOf((char) 160)), "") + " = " + cw[0] + "x4 - " + cw[5] + " = " + results[0] + "\n";
        info += subjects[1].replaceAll(String.valueOf(Character.valueOf((char) 160)), "") + " = " + cw[1] + "x4 - " + cw[6] + " = " + results[1] + "\n";
        info += subjects[2].replaceAll(String.valueOf(Character.valueOf((char) 160)), "") + " = " + cw[2] + "x4 - " + cw[7] + " = " + results[2] + "\n";
        info += subjects[3].replaceAll(String.valueOf(Character.valueOf((char) 160)), "") + " = " + cw[3] + "x4 - " + cw[8] + " = " + results[3] + "\n";
        info += subjects[4].replaceAll(String.valueOf(Character.valueOf((char) 160)), "") + " = " + cw[4] + "x4 - " + cw[9] + " = " + results[4] + "\n\n";
        info += getString(R.string.info_dialog_2);
        info += results[0] + " + 2x" + results[1] + " + 2x" + results[2] + " + " + results[3] +
                " + " + results[4] + " = " + calculator.calculate() + " " + getString(R.string.rt_part_2);
        info_text_view.setText(info);

        // NOTE: .replaceAll(String.valueOf(Character.valueOf((char) 160)), "") is for trimming &#160;
    }

    public void closeDialog(View view) {
        finish();
    }
}
