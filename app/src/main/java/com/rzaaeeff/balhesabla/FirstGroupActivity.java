package com.rzaaeeff.balhesabla;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FirstGroupActivity extends AppCompatActivity {
    private short c1, c2, c3, c4, c5;  // Correct answer count
    private short w1, w2, w3, w4, w5;  // Wrong answer count
    private short result = -1;

    boolean buttonVisible = false;
    Button saveButton, infoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_group);

        // Get all EditText elements and set filter for them. [Range 1-25]
        SetFilterForAllEditTextChildrenOfViewGroup allFilter =
                new SetFilterForAllEditTextChildrenOfViewGroup((ViewGroup) findViewById(R.id.g1_view_group));
        allFilter.setFilters(new InputFilterMinMax("0", "25"));

        saveButton = (Button) findViewById(R.id.save1);
        infoButton = (Button) findViewById(R.id.info1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Activity activity = this;
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(activity.getResources().getColor(R.color.groupStatusBarColor));
        }
    }

    /**
     * Get all values from EditTexts.
     *
     * TryCatch is not for parsing exceptions
     * Because, we've already set filters for TextView's,
     * So user will not be able to put any character other than [1-25]
     *
     * But, we use this to detect if the user kept any TextView empty.
     * If this occurs, we kindly ask if he/she wants us to fill those empty
     * TextViews with 0.
     *
     * It returns boolean, so we can know that operation was successful.
     * If operation was successful, we enable "Additional Information"&"Save" buttons.
     * @return
     */
    private boolean getValues() {
        EditText et;

        try {
            // Subject 1
            et = (EditText) findViewById(R.id.g1_s1_c_edit_text);
            c1 = Short.parseShort(et.getText().toString());
            et = (EditText) findViewById(R.id.g1_s1_w_edit_text);
            w1 = Short.parseShort(et.getText().toString());

            // Subject 2
            et = (EditText) findViewById(R.id.g1_s2_c_edit_text);
            c2 = Short.parseShort(et.getText().toString());
            et = (EditText) findViewById(R.id.g1_s2_w_edit_text);
            w2 = Short.parseShort(et.getText().toString());

            // Subject 3
            et = (EditText) findViewById(R.id.g1_s3_c_edit_text);
            c3 = Short.parseShort(et.getText().toString());
            et = (EditText) findViewById(R.id.g1_s3_w_edit_text);
            w3 = Short.parseShort(et.getText().toString());

            // Subject 4
            et = (EditText) findViewById(R.id.g1_s4_c_edit_text);
            c4 = Short.parseShort(et.getText().toString());
            et = (EditText) findViewById(R.id.g1_s4_w_edit_text);
            w4 = Short.parseShort(et.getText().toString());

            // Subject 5
            et = (EditText) findViewById(R.id.g1_s5_c_edit_text);
            c5 = Short.parseShort(et.getText().toString());
            et = (EditText) findViewById(R.id.g1_s5_w_edit_text);
            w5 = Short.parseShort(et.getText().toString());

            return true;
        } catch (Exception ex) {

            // Askiing user if he/she wants to fill all blank EditText views with 0 (zero).
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    new ContextThemeWrapper(this, R.style.customDialog));
            builder.setMessage(getString(R.string.question_2)).setCancelable(
                    false).setPositiveButton(getString(R.string.other_1),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ViewGroup vgParent = (ViewGroup) findViewById(R.id.g1_view_group);
                            for (int i = 0; i < vgParent.getChildCount(); i++) {
                                if (vgParent.getChildAt(i) instanceof ViewGroup) {
                                    ViewGroup vgChild = (ViewGroup) vgParent.getChildAt(i);
                                    for (int j = 0; j < vgChild.getChildCount(); j++) {
                                        if (vgChild.getChildAt(j) instanceof EditText) {
                                            if (((EditText) vgChild.getChildAt(j)).getText().toString().equals("")) {
                                                ((EditText) vgChild.getChildAt(j)).setText("0");
                                            }
                                        }
                                    }
                                }
                            }

                            Button saveButton = (Button) findViewById(R.id.calculate1);
                            saveButton.performClick();
                        }
                    }).setNegativeButton(getString(R.string.other_2),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

            return false;
        }
    }

    public void getResult1(View view) {
        String str;

        buttonVisible = getValues();
        str = CheckingMethods.checkAll(this, c1, c2, c3, c4, c5, w1, w2, w3, w4, w5);

        if (str.equals("NoErrors") && buttonVisible) {
            Calculator calculator = new Calculator(c1, c2, c3, c4, c5, w1, w2, w3, w4, w5);
            result = calculator.calculate();

            TextView result_view = (TextView) findViewById(R.id.g1_result_text_view);
            result_view.setText(getString(R.string.rt_part_1) + " " + result + " " + getString(R.string.rt_part_2));
        } else {
            // Displaying error
            if (!str.equals("NoErrors")) {
                Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
            }
            buttonVisible = false;
        }

        if (buttonVisible) {
            saveButton.setVisibility(View.VISIBLE);
            infoButton.setVisibility(View.VISIBLE);
        } else {
            saveButton.setVisibility(View.GONE);
            infoButton.setVisibility(View.GONE);
        }
    }

    public void openSaveDialog1(View view) {
        Intent saveIntent = new Intent(this, SaveDialog.class);
        saveIntent.putExtra("RESULT", String.valueOf(result));
        saveIntent.putExtra("GROUP", "1");
        startActivity(saveIntent);
    }

    public void openInfoDialog1(View view) {
        Intent infoIntent = new Intent(this, InfoDialogActivity.class);

        short[] cw = {c1, c2, c3, c4, c5,
                    w1, w2, w3, w4, w5};
        infoIntent.putExtra("CW", cw);

        String[] subjects = {getString(R.string.group1_subject1),
                getString(R.string.group1_subject2), getString(R.string.group1_subject3),
                getString(R.string.group1_subject4), getString(R.string.group1_subject5)};
        infoIntent.putExtra("SUBJECTS", subjects);

        startActivity(infoIntent);
    }
}
