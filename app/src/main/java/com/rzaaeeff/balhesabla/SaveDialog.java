package com.rzaaeeff.balhesabla;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

// TODO Preferences could be replaced by SQLite.
public class SaveDialog extends AppCompatActivity {

    final String PREF_NAME = "BalHesablaResults";

    private short result;
    private char group;

    /**
     * We've used Preferences, but we can use SQLite too.
     */
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private String nameText, groupText, resultText;

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_dialog);

        // Getting extras
        Intent intent = getIntent();
        result = Short.parseShort(intent.getStringExtra("RESULT"));
        String s = intent.getStringExtra("GROUP");
        group = s.charAt(0);

        editText = (EditText) findViewById(R.id.dialog_edit_text);
    }

    public void completeSaving(View view) {
        // Getting data from EditText
        String name = editText.getText().toString();

        // Getting SharedPreferences ready.
        preferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        nameText = preferences.getString("Name", "");
        groupText = preferences.getString("Group", "");
        resultText = preferences.getString("Result", "");
        editor = preferences.edit();

        if (!nameText.isEmpty()) nameText += "\n";
        if (!groupText.isEmpty()) groupText += "\n";
        if (!resultText.isEmpty()) resultText += "\n";
    
        if (!name.equals("")) {
            // Saving data
            nameText += name;
            switch (group) {
                case '1':
                    groupText += getString(R.string.option_1_text);
                    break;
                case '2':
                    groupText += getString(R.string.option_2_text);
                    break;
                case '3':
                    groupText += getString(R.string.option_3_text);
                    break;
                case '4':
                    groupText += getString(R.string.option_4_text);
                    break;
            }
            resultText += String.valueOf(result);
            editor.putString("Name", nameText);
            editor.putString("Group", groupText);
            editor.putString("Result", resultText);
            editor.commit();

            // Informing user.
            Toast.makeText(this, getString(R.string.info_1), Toast.LENGTH_SHORT).show();
            // Exiting
            finish();
        } else {
            Toast.makeText(this, getString(R.string.error_7), Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelSaving(View view) {
        editText.setText("");
        finish();
    }
}
