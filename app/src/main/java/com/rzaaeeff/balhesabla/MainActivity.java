package com.rzaaeeff.balhesabla;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Activity activity = this;
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(activity.getResources().getColor(R.color.mainStatusBarColor));
        }
    }

    public void openFirstGroupActivity(View view) {
        Intent fgIntent = new Intent(this, FirstGroupActivity.class);
        startActivity(fgIntent);
    }

    public void openSecondGroupActivity(View view) {
        Intent sgIntent = new Intent(this, SecondGroupActivity.class);
        startActivity(sgIntent);
    }

    public void openThirdGroupActivity(View view) {
        Intent tgIntent = new Intent(this, ThirdGroupActivity.class);
        startActivity(tgIntent);
    }

    public void openFourthGroupActivity(View view) {
        Intent fgIntent = new Intent(this, FourthGroupActivity.class);
        startActivity(fgIntent);
    }

    public void openResultsActivity(View view) {
        Intent rsIntent = new Intent(this, ResultsActivity.class);
        startActivity(rsIntent);
    }

    public void openSettingsActivity(View view) {
        Intent stIntent = new Intent(this, SettingsActivity.class);
        startActivity(stIntent);
    }

    public void exitApplication(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(this, R.style.customDialog));
        builder.setMessage(getString(R.string.question_1)).setCancelable(
                false).setPositiveButton(getString(R.string.other_1),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                }).setNegativeButton(getString(R.string.other_2),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
