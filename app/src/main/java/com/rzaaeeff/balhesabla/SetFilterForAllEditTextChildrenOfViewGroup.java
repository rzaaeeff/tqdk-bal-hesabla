package com.rzaaeeff.balhesabla;

import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by Rzaaeeff on 28.08.2016.
 */

public class SetFilterForAllEditTextChildrenOfViewGroup {
    private final int COUNT = 10;
    private ViewGroup mViewGroup;
    private ArrayList<View> mViewList;

    public SetFilterForAllEditTextChildrenOfViewGroup(ViewGroup viewGroup) {
        mViewGroup = viewGroup;
        mViewList = new ArrayList<View>();
    }

    /**
     * Getting all children of ViewGroup that are EditText.
     */
    private void getAllEditTextElements() {
        View currentView = null;

        for (int i = 0; i < mViewGroup.getChildCount(); i++) {
            if (mViewGroup.getChildAt(i) instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) mViewGroup.getChildAt(i);

                for (int j = 0; j < vg.getChildCount(); j++) {
                    currentView = vg.getChildAt(j);
                    if (currentView instanceof EditText) {
                        mViewList.add(currentView);
                    }
                }
            }
        }
    }

    /**
     * Setting filters.
     */
    public void setFilters(InputFilter inputFilter) {
        getAllEditTextElements();

        for (View view : mViewList) {
            EditText editText = (EditText) view;
            ((EditText) view).setFilters(new InputFilter[]{inputFilter});
        }
    }
}
